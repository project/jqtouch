api = 2
core = 6.x

libraries[jqtouch][download][type] = "git"
libraries[jqtouch][download][url] = "git://github.com/senchalabs/jQTouch.git"
libraries[jqtouch][download][branch] = "master"
libraries[jqtouch][directory_name] = "jqtouch"
libraries[jqtouch][destination] = "libraries"
