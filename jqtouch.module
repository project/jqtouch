<?php

/**
 * @file
 * Provides the jQTouch plug-in to other Drupal modules.
 *
 * This module doesn't do too much, but it is a central location for any other
 * modules that implement the jQTouch functionality. It ensures that multiple
 * modules will all include the same library script just once on any given page.
 */


/**
 * Implements of hook_menu().
 */
function jqtouch_menu() {
  $items['admin/settings/jqtouch'] = array(
    'title' => 'jQTouch',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('jqtouch_admin_settings_themes'),
    'access arguments' => array('administer site configuration'),
    'weight' => 0,
  );
  return $items;
}

/**
 * Form builder. Configure jQTouch.
 * @ingroup forms
 * @see system_settings_form()
 */
function jqtouch_admin_settings_themes() {
  $themes = jqtouch_get_themes();
  $form['jqtouch_themes'] = array(
    '#type' => 'radios',
    '#title' => t('Themes'),
    '#options' => $themes,
    '#default_value' => variable_get('jqtouch_themes', 0),
  );

  return system_settings_form($form);
}

/**
 * Implements of hook_jq().
 */
function jqtouch_jq($op, $plugin = NULL) {
  switch ($op) {
    case 'info':
      return array(
        'jqtouch' => _jqtouch_plugin_info(),
      );
  }
}

/**
 * Helper function to get information about the plugin
 * @return
 *   An array containg the information about jQTouch
 */
function _jqtouch_plugin_info() {
  $themes = jqtouch_get_themes();
  return array(
    'name' => t('jQTouch'),
    'description' => t('A jQuery plugin for mobile web development on the iPhone, iPod Touch, and other forward-thinking devices.'),
    'version' => jqtouch_get_version(),
    'url' => 'https://github.com/senchalabs/jQTouch/archives/master',
    'files' => array(
      'js' => array(
        libraries_get_path('jqtouch') . '/jqtouch/jqtouch.js',
      ),
      'css' => array(
        libraries_get_path('jqtouch') . '/jqtouch/jqtouch.css',
        libraries_get_path('jqtouch') . '/themes/' . $themes[variable_get('jqtouch_themes', 0)] . '/theme.css',
      ),
    ),
  );
}

/**
 * Helper function to add the library if jQ is not installed
 * 
 * @param $options
 *   Options to be given to drupal_add_js and drupal_add_css
 */
function jqtouch_add($options = array()) {
  // Set the default options
  $options['js'] = isset($options['js']) ? $options['js'] : array();
  $options['js']['type'] = isset($options['js']['type']) ? $options['js']['type'] : 'module';
  $options['js']['scope'] = isset($options['js']['scope']) ? $options['js']['scope'] : 'header';
  $options['js']['defer'] = isset($options['js']['defer']) ? $options['js']['defer'] : FALSE;
  $options['js']['cache'] = isset($options['js']['cache']) ? $options['js']['cache'] : TRUE;
  $options['js']['preprocess'] = isset($options['js']['preprocess']) ? $options['js']['preprocess'] : TRUE;
  $options['css'] = isset($options['css']) ? $options['css'] : array();
  $options['css']['type'] = isset($options['css']['type']) ? $options['css']['type'] : 'module';
  $options['css']['media'] = isset($options['css']['media']) ? $options['css']['media'] : 'all';
  $options['css']['preprocess'] = isset($options['css']['preprocess']) ? $options['css']['preprocess'] : TRUE;

  // Get the files to load
  $info = _jqtouch_plugin_info();
  foreach ($info['files']['js'] as $file) {
    drupal_add_js($file, $options['js']['type'], $options['js']['scope'], $options['js']['defer'], $options['js']['cache'], $options['js']['preprocess']);
  }
  foreach ($info['files']['css'] as $file) {
    drupal_add_css($file, $options['css']['type'], $options['css']['media'], $options['css']['preprocess']);
  }
}

/**
 * Helper function to get version of jQTouch installed.
 * 
 * @return
 *   The revision number of the plugin.
 */
function jqtouch_get_version() {
  $version = 0;

  if (file_exists(libraries_get_path('jqtouch') . '/jqtouch/jqtouch.js')) {
    $version = file_get_contents(libraries_get_path('jqtouch') . '/jqtouch/jqtouch.js');
    $matches = array();
    if (preg_match('/\$Revision:\s*(\d+).*\$/i', $version, $matches)) {
      if (count($matches) > 1) {
        $version = $matches[1];
      }
    }
  }

  return $version;
}

/**
 * Return the themes in jQTouch themes folder
 * 
 * @return
 *   The folder names in the themes folder of the plugin.
 * 
 * @param $exclusions
 *   An array containing the folders in the themes dir
 *   to ignore and not consider as a theme.
 */
function jqtouch_get_themes($exclusions = NULL) {
  if (is_null($exclusions)) {
    $exclusions = array(
      '.',
      '..',
      'CVS',
      '.git',
    );
  }
  $themes = array();
  $path = getcwd() . '/' . libraries_get_path('jqtouch') . '/themes';
  if ($handle = opendir($path)) {
    while (false !== ($file = readdir($handle))) {
      if (is_dir($path . '/' . $file) && !in_array($file, $exclusions)) {
        $themes[] = $file;
      }
    }
    closedir($handle);
  }
  return $themes;
}