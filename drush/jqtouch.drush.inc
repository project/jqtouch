<?php

/**
 * @file
 *   drush integration for jqtouch.
 */

/**
 * The jQTouch plugin URI.
 */
define('JQTOUCH_DOWNLOAD_URI', 'git://github.com/senchalabs/jQTouch.git');

/**
 * Implementation of hook_drush_command().
 *
 * In this hook, you specify which commands your
 * drush module makes available, what it does and
 * description.
 *
 * Notice how this structure closely resembles how
 * you define menu hooks.
 *
 * @See drush_parse_command() for a list of recognized keys.
 *
 * @return
 *   An associative array describing your command(s).
 */
function jqtouch_drush_command() {
  $items = array();

  // the key in the $items array is the name of the command.
  $items['jqtouch-plugin'] = array(
    'callback' => 'drush_jqtouch_plugin',
    'description' => dt("Downloads the jQTouch plugin."),
    'arguments' => array(
      'path' => dt('Optional. A path where to install the jQTouch plugin. If omitted Drush will use the default location.'),
    ),
  );

  return $items;
}

/**
 * Implementation of hook_drush_help().
 *
 * This function is called whenever a drush user calls
 * 'drush help <name-of-your-command>'
 *
 * @param
 *   A string with the help section (prepend with 'drush:')
 *
 * @return
 *   A string with the help text for your command.
 */
function jqtouch_drush_help($section) {
  switch ($section) {
    case 'drush:jqtouch-plugin':
      return dt("Downloads the jQTouch plugin from github.com/senchalabs, default location is sites/all/libraries.");
  }
}

/**
 * Implements drush_MODULE_post_pm_enable().
 */
// function drush_jqtouch_post_pm_enable() {
//   $modules = func_get_args();
//   if (in_array('jqtouch', $modules)) {
//     drush_jqtouch_plugin();
//   }
// }

/**
 * Command to download the jQTouch plugin.
 */
function drush_jqtouch_plugin() {
  $args = func_get_args();
  if ($args[0]) {
    $path = $args[0];
  }
  else {
    $path = 'sites/all/libraries';
  }

  // Create the path if it does not exist.
  if (!is_dir($path)) {
    drush_op('mkdir', $path);
    drush_log(dt('Directory @path was created', array('@path' => $path)), 'notice');
  }

  // Set the directory to the download location.
  $olddir = getcwd();
  chdir($path);

  $filename = basename(JQTOUCH_DOWNLOAD_URI);
  $dirname = basename(JQTOUCH_DOWNLOAD_URI, '.git');

  // Remove any existing jQTouch plugin directory
  if (is_dir($dirname)) {
    drush_log(dt('A existing jQTouch plugin was overwritten at @path', array('@path' => $path)), 'notice');
  }

  // Download the archive
  if (!drush_shell_exec('git clone '. JQTOUCH_DOWNLOAD_URI . ' jqtouch')) {
    drush_log(dt('Error cloning the master branch from Git. Do you have git installed?'), 'error');
  }

  // Set working directory back to the previous working directory.
  chdir($olddir);

  if (file_exists($path .'/jqtouch/jqtouch/jqtouch.js')) {
    drush_log(dt('jQTouch plugin has been downloaded to @path', array('@path' => $path)), 'success');
  }
  else {
    drush_log(dt('Drush was unable to download the jQTouch plugin to @path', array('@path' => $path)), 'error');
  }
}
